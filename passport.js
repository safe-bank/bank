/**
 * Created by LEYNA on 30.11.2014.
 */
// config/passport.js

/*jslint node: true */
"use strict";

var mysql = require('mysql');
var sqlInfo = require('./config/dbconfig.js');
var connection = mysql.createConnection(sqlInfo, function(err){
	if(err) console.error("Error connecting to DB: " +err);
});
var LocalStrategy = require('passport-local').Strategy;
var bCrypt = require('bcrypt-nodejs');
//var loader =;
require('./logger');

module.exports = function (app, passport) {
	passport.use('login', new LocalStrategy({
			usernameField: 'login',
			passwordField: 'password',
			passReqToCallback: true
		},
		function (req, login, password, done) {

			var sql = "SELECT * from users WHERE login=? limit 1";
			connection.query(sql, login, function (err, result) {
			console.log("using login strategy");
			console.log("login: " + login);
			console.log("password: " + password);
			var sql = "SELECT * from users WHERE login='" + login + "' limit 1";
			connection.query(sql, function (err, result) {
//				console.log(result[0].password_hash);

				if (err) {
					console.log("Error checking user: " + err.message);
					return done(null, false,
					req.flash('loginMessage', 'Unexpected error occured. Please try again later.'));
				}   else
				if (!result.length) {
					console.log('User not found: ' + login);
					return done(null, false,
						req.flash('loginMessage', 'User not found. Check login'));
				}
				if (!bCrypt.compareSync(password,result[0].password_hash)) {
					console.log('Wrong password');
					return done(null, false,
						req.flash('loginMessage', 'Incorrect password'));
				} else {
					var res = result[0];
					res.accounts = [];
					var query = "SELECT * from account where user_owner_ID = ?";
					connection.query(query,res.id,function(err, rows){
						if(err) {console.log("Error loading account"); throw err;}
						else {
							for(var i=0, len=rows.length; i<len; i++){
								res.accounts.push(rows[i]);
							}

							passport.serializeUser(function (res, done) {
								done(null, res);
							});
							passport.deserializeUser(function (res, done) {
								done(null, res);
							});
							return done(null, res);
							}
					});
				    }
			});
		})
	}));

	passport.use('signup', new LocalStrategy({
			usernameField: 'login',
			passwordField: 'password',
			passReqToCallback: true
		},
		function (req, login, password, done) {
			var connection = mysql.createConnection(sqlInfo);
			var sql_check = "SELECT login, email from users WHERE login=? OR email = ? limit 1";
			console.log(sql_check);
			connection.query(sql_check, [login, req.param('email')], function (err, rows) {
				if (err) {
					console.error("Error checking user: "+err.message);
					return done(null, false, req.flash('signupMessage', 'Sorry, internal error. Try later.'));
				}
				if (rows.length) {
					console.log(rows[0].login);
					console.log(login);
					console.log(login==rows[0].login);
					if(login==rows[0].login){
						console.log(true);
						return done(null, false, req.flash('signupMessage', 'This login already in use.'));
					}

				if(req.param('email')==rows[0].email){
					return done(null, false, req.flash('signupMessage', 'This email already in use.'));
				}}
				else {
					var salt = bCrypt.genSaltSync();
					var user = new Object({
						login: login,
						password_hash: bCrypt.hashSync(password,salt),
						password_salt: salt,
						email: req.param('email'),
						first_name: req.param('firstName'),
						middle_name: req.param('middleName'),
						last_name: req.param('lastName'),
						role: 'user'
					});
					var userData = [];
					for(var prop in user){
						userData.push(user[prop]);
					}
					var insertQueryUser = "INSERT INTO users (login, password_hash, password_salt, email, first_name, middle_name, last_name, role) VALUES (?,?,?,?,?,?,?,?)";
					insertQueryUser = mysql.format(insertQueryUser, userData);
					connection.query(insertQueryUser, function (err, rows) {
						if (err) {
							return done(err, false, req.flash('signupMessage', 'Some error occured, please try again later.'));
						} else {
							user.id = rows.insertId;
							user.accounts = [];

							var accountData = [req.param('curr'), user.id];
							var insertQueryAccount = "INSERT INTO account (money_type, status, balance, user_owner_ID) VALUES (?, 'active', 0.00, ?)";
							connection.query(insertQueryAccount, accountData, function(err){
								if(err) {
									return done(err, false, req.flash('signupMessage', 'Some error occured, please try again later.'));
								}
								else {
									var query = "SELECT * from account where user_owner_ID = ?";
									connection.query(query,user.id,function(err, rows){
										if(err) {console.log("Error loading account"); throw err;}
										else {
											user.accounts.push( rows[0]);
											passport.serializeUser(function (user, done) {
												done(null, user);
											});

											passport.deserializeUser(function (user, done) {
												done(null, user);
											});
											console.log(user);
											return done(null, user);
										}
									});
								}
							});
						}
					});
				}
			});
		}));
};
