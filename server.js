/**
 * Created by LEYNA on 30.11.2014.
 */

/*jslint node: true */
"use strict";

var express = require('express');
var port = 3000;
var passport = require('passport');
var flash = require('connect-flash');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mysql = require('mysql');
var session = require('express-session');
var domain = require('domain');
var morgan = require('morgan');
var sqlInfo = require('./config/dbconfig.js');
var connection = mysql.createConnection(sqlInfo, function (err) {
	if (err) console.error("Error connecting to DB: " + err);
});
global.connection = connection;
require('./logger');
require('./currency');
var serverDomain = domain.create();
serverDomain.on('error', function (err) {
	console.log("Server Domain Error: " + err);
	res.end("Server error");
});

var app = express();

serverDomain.add(app);

app.use(express.static(__dirname + '/public'));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true
}));
app.set('view engine', 'ejs');
require('./passport.js')(app, passport);
app.use(session({
	secret: 'thesecret',
	resave: false,
	saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
//app.use(morgan);
//app.use(express.logger('dev'));
app.use(function (err, req, res, next) {
	console.log("Processing request");
	var requestDomain = domain.create();
	requestDomain.add(req);
	requestDomain.add(res);
	requestDomain.on('error', function (err) {
		console.log("Caught in domain " + app.process.domain.id);
		res.end("Sorry, we got a mistake processing your request.");
		requestDomain.dispose();
		next(err);
	});
	requestDomain.run(next);
});
app.listen(port, function () {
	console.log('Express server listening on port ' + port);

});
// app/routes.js - желательно вынести в отдельный файл

app.get('/', function (req, res) {
	res.render('index.ejs', {
		user: req.user
	});
});
app.get('/login', function (req, res) {

	// render the page and pass in any flash data if it exists
	res.render('login.ejs', {
		message: req.flash('loginMessage')
	});

});
app.post('/login', passport.authenticate('login', {
	successRedirect: '/profile',
	failureRedirect: '/login',
	failureFlash: true

}));
app.get('/signup', function (req, res) {
	res.render('signup.ejs', {
		message: req.flash('signupMessage')
	});
});
app.post('/signup', passport.authenticate('signup', {
	successRedirect: '/profile',
	failureRedirect: '/signup',
	failureFlash: true
}));
app.get('/fail', function (req, res) {
	res.render('fail.ejs');
});
app.get('/profile', function (req, res) {
	if (req.isAuthenticated()) {
		if(req.user.role=='admin'){}
		else{
		connection.query("SELECT * FROM account WHERE user_owner_ID=?", req.user.id, function (err, rows) {
			if (err) {
				res.end("Error loading your account");
			} else {
				for (var i = 0, len = req.user.accounts.length; i < len; i++) {
					req.user.accounts[i] = rows[i];
				}
				connection.query("SELECT * FROM log WHERE user_ID =?",req.user.id,function(err, rows){
					var messages = [];
					for(var i = 0, len = rows.length; i < len; i++){
						messages.push(rows[i].action_info);}
					res.render('profile.ejs', {
						user: req.user,
						accountMessage : req.flash('accountMessage'),
						messages: messages
					});
				});

			}
		});}
	} else res.redirect('/');
});
app.post('/profile', function(req, res) {
	var accountData = [req.param('curr'), req.user.id];
	var insertQueryAccount = "INSERT INTO account (money_type, status, balance, user_owner_ID) VALUES (?, 'active', 0.00, ?)";
	connection.query(insertQueryAccount, accountData, function (err, rows) {
		if (err)
		{req.flash('accountMessage', 'Some error occured, please try again later.');}
		else
		{req.flash('accountMessage', 'You have created new account succesfully. Please relog to get access.');
			res.redirect('/profile');
			logger(req.user.id,rows.insertId,"New account created");
		}

});
});


app.get('/logout', function (req, res) {
	req.logOut();
	res.redirect('/');
});

app.get('/account/:id?', function (req, res) {
	if (req.isAuthenticated()) {
		var id = req.param("id");
		var query = "SELECT * FROM account WHERE account_ID = " + id + " limit 1";
		connection.query(query, function (err, rows) {
			if (err) {
				req.flash('message', "Error loading your account");
			} else {
				if (req.user.id != rows[0].user_owner_ID) {
					res.end("Dont you mess with URL to access random accounts! No way!");
				} else {
					res.render('account', {
						acc: rows[0],
						depositMessage: req.flash('dMessage'),
						cashMessage: req.flash('cMessage'),
						currMessage: req.flash('currMessage'),
						transferMessage: req.flash('tMessage')
					});
				}
			}

		});
	} else res.redirect('/');
});
app.post('/account/:id?/deposit', function (req, res) {
	var id = req.param("id");
	connection.beginTransaction(function (err) {
		if (err) {
			req.flash('dMessage', 'We got an error beginning transaction');
			res.redirect('/account/' + id);
			console.log(err);
		} else {
			var query = "SELECT * FROM account WHERE account_ID = " + connection.escape(id) + " limit 1";
			connection.query(query, id, function (err, rows) {
				if (err) {
					req.flash('dMessage', 'We got an error checking account. Sorry.');
					res.redirect('/account/' + id);
					console.log(err);
				} else {
					var amount = parseFloat(req.param('amount'));
					var balance = parseFloat(rows[0].balance);
					var curr = rows[0].money_type;
					if (0.01 * amount > 1000) balance = balance - 1000 + amount;
					else balance = balance + 0.99 * amount;
					var query = "UPDATE account SET balance = " + balance + " WHERE account_ID = " + connection.escape(id);
					connection.query(query, balance, function (err, rows) {
						if (err) {
							connection.rollback();
							req.flash('dMessage', 'We got an error updating data. Sorry.');
							res.redirect('/account/' + id);
							console.log(err);
						} else {
							connection.commit();
							req.flash('dMessage', "You have updated your account successfully.");
							res.redirect('/account/' + id);
							logger(req.user.id,id,"Deposit money on account "+id+", "+amount+" "+curr);
						}
					});
				}
			});
		}
	});

});


app.post('/account/:id?/cash', function (req, res) {
	var id = req.param("id");
	connection.beginTransaction(function (err) {
		if (err) {
			req.flash('cMessage', 'We got an error beginning transaction');
			res.redirect('/account/' + id);
			console.log(err);
		} else {
			var query = "SELECT * FROM account WHERE account_ID = " + connection.escape(id) + " limit 1";
			connection.query(query, id, function (err, rows) {
				if (err) {
					req.flash('cMessage', 'We got an error checking account. Sorry.');
					res.redirect('/account/' + id);
					console.log(err);
				} else {
					var balance = parseFloat(rows[0].balance);
					var amount = parseFloat(req.param('amount'));
					var cash = amount;
					if ((0.01 * amount) > 1000) cash = 1000 + amount;
					else cash = 1.01 * amount;
					var curr = rows[0].money_type;
					if (balance < cash) {
						connection.rollback();
						req.flash('cMessage', 'You dont have enough money for that.');
						res.redirect('/account/' + id);
					} else {
						balance = balance - cash;
						var query = "UPDATE account SET balance = " + balance + " WHERE account_ID = " + connection.escape(id);
						connection.query(query, balance, function (err) {
							if (err) {
								connection.rollback();
								req.flash('cMessage', 'We got an error updating data. Sorry.');
								res.redirect('/account/' + id);
								console.log(err);
							} else {
								connection.commit();
								req.flash('cMessage', "You have cashed your money successfully.");
								logger(req.user.id,id,"Cashed money from account "+id+", "+amount+" "+curr);
								res.redirect('/account/' + id);
							}
						});
					}
				}
			});
		}
	});
});


app.post('/account/:id?/currency', function (req, res) {
	var id = req.param("id");
	connection.beginTransaction(function (err) {
		if (err) {
			req.flash('currMessage', 'We got an error beginning transaction');
			res.redirect('/account/' + id);
			console.log(err);
		} else {
			var query = "SELECT * FROM account WHERE account_ID = " + connection.escape(id) + " limit 1";
			connection.query(query, id, function (err, rows) {
				if (err) {
					req.flash('currMessage', 'We got an error checking account. Sorry.');
					res.redirect('/account/' + id);
					console.log(err);
				} else {
					var money = rows[0].balance;
					var curr1 = rows[0].money_type;
					var curr2 = req.param("curr");
					if(curr1==curr2) {
						connection.rollback();
						req.flash('currMessage', 'No changes was made.');
						res.redirect('/account/' + id);
					}
					else{
					getRatio(curr1,curr2, function(a){
						money = money * parseFloat(a);

					var query = "UPDATE account SET money_type = " + connection.escape(req.param("curr")) + ", balance = "+connection.escape(money)+" WHERE account_ID = " + connection.escape(id);
					connection.query(query, function (err) {
						if (err) {
							connection.rollback();
							req.flash('currMessage', 'We got an error updating data. Sorry.');
							res.redirect('/account/' + id);
							console.log(err);
						} else {
							connection.commit();
							req.flash('currMessage', "You have changed your currency succesfully");
							logger(req.user.id,id,"Changed currency on account "+id+", from "+curr1+" to "+curr2);
							res.redirect('/account/' + id);
						}
					});
					});
					}
				}
			});
		}
	});
});


app.post('/account/:id?/transfer', function (req, res) {
	var id = req.param("id");
	connection.beginTransaction(function (err) {
		if (err) {
			req.flash('tMessage', 'We got an error beginning transaction');
			res.redirect('/account/' + id);
			console.log(err);
		} else {
			var query = "SELECT * FROM account WHERE account_ID = " + connection.escape(id) + " limit 1";
			connection.query(query, id, function (err, rows) {
				function send() {
					var sender_ID = id;
					var sender_balance = parseFloat(rows[0].balance);
					var amount_to_give = parseFloat(req.param('amount'));
					var amount_to_take = amount_to_give;
					if ((0.01 * amount_to_give) > 1000) amount_to_take = 1000 + amount_to_give;
					else amount_to_take = 1.01 * amount_to_give;
					var curr1 = rows[0].money_type;
					if (sender_balance < amount_to_take) {
						connection.rollback();
						req.flash('tMessage', 'You dont have enough money for that.');
						res.redirect('/account/' + id);
					} else {
						sender_balance = sender_balance - amount_to_take;
						var recipient_ID = req.param("recipient_ID");
						var query = "SELECT * FROM account WHERE account_ID = " + connection.escape(recipient_ID) + " limit 1";
						connection.query(query, function (err, rows) {
							if (!rows.length) {
								connection.rollback();
								req.flash('tMessage', 'Account does not exist.');
								res.redirect('/account/' + id);
							} else {
								var curr2 = rows[0].money_type;
								var owner = rows[0].user_owner_ID;
								getRatio(curr1, curr2, function(ratio){
									amount_to_give = amount_to_give*parseFloat(ratio);
									var recipient_balance = parseFloat(rows[0].balance) + amount_to_give;
								var query = "UPDATE account SET balance = " + connection.escape(sender_balance) + " WHERE account_ID = " + connection.escape(id);
								connection.query(query, function (err) {
									if (err) {
										connection.rollback();
										req.flash('tMessage', 'We got an error updating data. Sorry.');
										res.redirect('/account/' + id);
										console.log(err);
									} else {
										var query = "INSERT INTO transaction (sender_account_ID, reciever_account_ID, sender_sum_before, sender_sum_after, reciever_sum_before, reciever_sum_after) VALUES (" + connection.escape(sender_ID) + "," + connection.escape(recipient_ID) + "," + connection.escape(sender_balance + amount_to_take) + "," + connection.escape(sender_balance) + "," + connection.escape(recipient_balance - amount_to_give) + "," + connection.escape(recipient_balance) + ")";
										connection.query(query, function (err) {
											if (err) {
												connection.rollback();
												req.flash('tMessage', 'We got an error updating data. Sorry.');
												res.redirect('/account/' + id);
												console.log(err);
											} else {
												var query = "UPDATE account SET balance = " + connection.escape(recipient_balance) + " WHERE account_ID = " + connection.escape(recipient_ID);
												connection.query(query, function (err) {
													if (err) {
														connection.rollback();
														req.flash('tMessage', 'We got an error updating data. Sorry.');
														res.redirect('/account/' + id);
														console.log(err);
													} else {
														connection.commit();
														req.flash('tMessage', "Your money have been transferred successfully.");
														logger(req.user.id, id,"Sent money to account "+recipient_ID+", "+amount_to_take+" "+curr1);
														logger(owner, recipient_ID,"Received money from account "+sender_ID+", "+amount_to_give+" "+curr2);
														res.redirect('/account/' + id);
													}
												});
											}
										});
									}
								});
									});
							}
						});
					}
				}

				if (err) {
					req.flash('tMessage', 'We got an error checking account. Sorry.');
					res.redirect('/account/' + id);
					console.log(err);
				} else {
					send();
				}
			});
		}
	});
});


